#!/usr/bin/python
# motion.py
"""
Defines a Motion object which will continuously poll the sensor GY521 for accelerometer and gyroscope data and integrates angular velocity for angular displacement.

theta_x and theta_y work, but theta_z may not be correct

"""

import smbus
import math
import time
import threading

# Power management registers
POWER_MGMT_1 = 0x6b
POWER_MGMT_2 = 0x6c

GYRO_SCALE = 131.0 #gives angular velocity in degrees/sec
ACCEL_SCALE = 16384.0  #gives linear acceleration in a multiple of g (9.8ms^-2)

ADDRESS = 0x68  # This is the address value read via the i2cdetect command

DELAY = 0.01

K0 = 0.98 #proportion of angle which comes from gyro
K1 = 1 - K0 #proportion of angle which comes from accelerometer

bus = smbus.SMBus(1)
# Now wake the 6050 up as it starts in sleep mode
bus.write_byte_data(ADDRESS, POWER_MGMT_1, 0)

def _twos_compliment(val):
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val

def dist(a, b):
    return math.sqrt((a * a) + (b * b))

def _acceleration_to_rotation(a_x,a_y,a_z):
    theta_y = -math.degrees(math.atan2(a_x, dist(a_y,a_z)))
    theta_x = math.degrees(math.atan2(a_y, dist(a_x,a_z)))
    theta_z = -math.degrees(math.atan2(a_z, dist(a_y,a_x)))
    return (theta_x, theta_y, theta_z)

def _read_angular_velocity():
    #omega is the angular velocity, currently in degrees/sec
        
    #Read raw data from sensors
    raw_gyro_data = bus.read_i2c_block_data(ADDRESS, 0x43, 6)

    #scale gyro data
    omega_x = _twos_compliment((raw_gyro_data[0] << 8) + raw_gyro_data[1]) / GYRO_SCALE
    omega_y = _twos_compliment((raw_gyro_data[2] << 8) + raw_gyro_data[3]) / GYRO_SCALE
    omega_z = _twos_compliment((raw_gyro_data[4] << 8) + raw_gyro_data[5]) / GYRO_SCALE

    return (omega_x, omega_y, omega_z)

def _read_acceleration():
    #a is linear acceleration, currently in gs (ie. multiples of 9.8ms^-1)
        
    #Read raw data from sensors
    raw_accel_data = bus.read_i2c_block_data(ADDRESS, 0x3b, 6)

    #scale accelerometer data
    a_x = _twos_compliment((raw_accel_data[0] << 8) + raw_accel_data[1]) / ACCEL_SCALE
    a_y = _twos_compliment((raw_accel_data[2] << 8) + raw_accel_data[3]) / ACCEL_SCALE
    a_z = _twos_compliment((raw_accel_data[4] << 8) + raw_accel_data[5]) / ACCEL_SCALE

    return (a_x, a_y, a_z)


class Motion(object):
    def __init__(self):
        self.calibrate()
        self.synchronize()

    def synchronize(self):
        """
            This function is called continuously and updates the state variables
            by reading the sensors
        """
        
        #omega is the angular velocity, currently in degrees/sec
        #a is linear acceleration, currently in gs (ie. multiples of 9.8ms^-1)
        (self.omega_x, self.omega_y, self.omega_z) = _read_angular_velocity()
        (self.a_x, self.a_y, self.a_z) = _read_acceleration()

        #Update time variables
        delta_t = time.time() - self.current_time
        self.current_time = time.time()

        #integrate angular velocity omega, for angular displacement theta in degrees
        gyro_theta_x = (self.omega_x*delta_t) + self.theta_x
        gyro_theta_y = (self.omega_y*delta_t) + self.theta_y
        gyro_theta_z = (self.omega_z*delta_t) + self.theta_z

        (accel_theta_x, accel_theta_y, accel_theta_z) = _acceleration_to_rotation(self.a_x,self.a_y,self.a_z)

        #set angle values to proportionally use both gyro and accelerometer data
        self.theta_x = K0*gyro_theta_x + K1*(accel_theta_x - self.start_theta_x)
        self.theta_y = K0*gyro_theta_y + K1*(accel_theta_y - self.start_theta_y)
        self.theta_z = K0*gyro_theta_z + K1*(accel_theta_z - self.start_theta_z)

        #to calculate v and x, must remove acceleration due to gravity, using the current angle to figure out components of g
        #in all 3 directions!!!
        
        #ensure synchronisation happens regularly
        t = threading.Timer(DELAY, self.synchronize)
        t.start()

    def calibrate(self):
        #assumes sensor is stationary when this function is called
        self.start_time = time.time()
        self.current_time = self.start_time

        (self.theta_x, self.theta_y, self.theta_z) = (0, 0, 0)

        (a_x, a_y, a_z) = _read_acceleration()
        
        #start theta x, y and z are used to correct for the acceleration angle
        #being measured from a different origin to the gyro angle
        (self.start_theta_x, self.start_theta_y, self.start_theta_z) = _acceleration_to_rotation(a_x,a_y,a_z)

    def get_acceleration(self):
        return(self.current_time - self.start_time, self.a_x, self.a_y, self.a_z)
    
    def get_angular_velocity(self):
        return(self.current_time - self.start_time, self.omega_x, self.omega_y, self.omega_z)

    def get_angular_displacement(self):
        return(self.current_time - self.start_time, self.theta_x, self.theta_y, self.theta_z)


