#!/usr/bin/python
#
# Provides functions to measure distance
# Using a HC-SR04 sensor with a raspberry pi
#
# Original Author : Matt Hawkins
# Edited By: Matthew Di Meglio
# -----------------------

import time
import RPi.GPIO as GPIO

# Define GPIO to use on Pi
GPIO_TRIGGER = 23
GPIO_ECHO    = 24

SPEED_OF_SOUND = 34300 #cm/sec

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

# Set pins as output and input
GPIO.setup(GPIO_TRIGGER,GPIO.OUT)  # Trigger
GPIO.setup(GPIO_ECHO,GPIO.IN)      # Echo

# Set trigger to False (Low)
GPIO.output(GPIO_TRIGGER, False)


def get_distance():
    # This function measures a distance
    
    GPIO.output(GPIO_TRIGGER, True)
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)
    start = time.time()
    
    #wait until echo signals the pulse has been sent
    while GPIO.input(GPIO_ECHO)==0:
        start = time.time()
    
    #wait until echo signals the pulse has been recieved
    while GPIO.input(GPIO_ECHO)==1:
        stop = time.time()
    
    #calculate distance: half the distance the sound pulse travelled
    elapsed = stop-start
    distance = (elapsed * SPEED_OF_SOUND)/2
                
    return distance

def get_distance_average(numSamples):
    # This function takes numSamples measurements and
    # returns the average.
    distanceSum = 0
    i = 0
    while i < numSamples:
        distanceSum += get_distance()
        time.sleep(0.1)
        i = i + 1
    return distanceSum/numSamples

def cleanup():
    GPIO.cleanup()

