import ultrasonic
import time
import math
from mantis import Mantis
from motion import Motion

robot = Mantis()
sensors = Motion()

#in centimetres
MAX_DIST_TO_STOP = 10
MAX_DIST_TO_TURN = 50
SPAN_DIST_TO_TURN = (MAX_DIST_TO_TURN - MAX_DIST_TO_STOP)

#in seconds
WAIT_TIME_INTERVAL = 0.3
WAIT_PERPENDICULAR = 5

NORMAL_VELOCITY_SIZE = 0.5
MIN_VELOCITY_SIZE_PROPORTION = 0.5

# Wrap main content in a try block so we can
# catch the user pressing CTRL-C and run the
# GPIO cleanup function. This will also prevent
# the user seeing lots of unnecessary error
# messages.
try:
    velocitySize = NORMAL_VELOCITY_SIZE
    turnSize = 0
    a_curr = sensors.get_acceleration()
    
    while True:
        #return distance detected by U.S. sensor
        distance = ultrasonic.measure()
        print ("Distance: %.lf" % distance)
    
    
    #tidy up folowing if it works
        a_prev = a_curr
        a_curr = sensors.get_acceleration()
        
        #jerk is time derivative of acceleration
        for i in range(1, 3):
            jerk[i] = (a_curr[i] - a_prev[i])/(a_curr[0] - a_prev[0])
        
        jerk = math.sqrt(jerk[0]*jerk[0] + jerk[1]*jerk[1] + jerk[2]*jerk[2])
        
        
        #do something based on jerk in x, y and z directions, move in same direction to the jerk???
        #does this accomplish anything useful? if we hit in any direction
        #except front and back, we are likely on our side (flipped) and can't do anything
        #else the distance < MAX_DIST_TO_STOP will save us
        #else we're trapped on the front and back and can't do anything
        
        #also, if our angle is changing, our ultrasonic sensor will likely see ground/ceiling and this will affect how it moves (might stop when can go forward - perhaps tape ultrasonic to lifting front
        #then if dist<MAX_DIST_TO_STOP, lift front so that it is horizontal and see if dist still < MAX_DIST_STOP
        
        if distance < MAX_DIST_TO_STOP || jerk > 0.2:
            print ("Stop and reverse")
            #stop moving
            robot.set_velocity_size(0)
            
            #turn and reverse
            turnSize = 1
            robot.turn_to(turnSize)
            robot.set_velocity_size(-NORMAL_VELOCITY_SIZE)
            time.sleep(WAIT_PERPENDICULAR)

            #reset
            turnSize = 0
            velocitySize = NORMAL_VELOCITY_SIZE
        
            a_prev = a_curr
            a_curr = sensors.get_acceleration()
 
        
        elif distance < MAX_DIST_TO_TURN:
            print ("Slowly turn")
            #turn size proportional to closeness of object
            turnSize = 1 - (distance - MAX_DIST_TO_STOP)/SPAN_DIST_TO_TURN
            
            #velocity size inversely propritional to distance to object,
            #max velocity is NORMAL_VELOCITY_SIZE and min is MIN_VELOCITY_SIZE_PROPORTION*NORMAL_VELOCITY_SIZE
            velocitySize = NORMAL_VELOCITY_SIZE*(MIN_VELOCITY_SIZE_PROPORTION + (1 - MIN_VELOCITY_SIZE_PROPORTION)*((distance - MAX_DIST_TO_STOP)/SPAN_DIST_TO_TURN))
        else:
            print ("Go straight")
            #go straight
            turnSize = 0
        print("Turn size: {} Velocity size: {}".format(turnSize, velocitySize))
        robot.turn_to(turnSize)
        robot.set_velocity_size(velocitySize)
        
        time.sleep(WAIT_TIME_INTERVAL)

except KeyboardInterrupt:
  # User pressed CTRL-C
  # Reset GPIO settings
  robot.set_velocity_size(0)
  robot.turn_to(0)
  ultrasonic.cleanup()





