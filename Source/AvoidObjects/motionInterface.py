#this file provides an interface to motion.py
#it continuously prints out all the data
#it was used to test whether motion.py works

from motion import Motion
import time

DELAY = 0.2

sensors = Motion()

def print_data():
    print("")
    (time, a_x, a_y, a_z) = sensors.get_acceleration()
    print("Time:{0:.2f}\t\tAccel x:{1:.2f}\t\ty:{2:.2f}\t\tz:{3:.2f}".format(time, a_x, a_y, a_z))

    (time, omega_x, omega_y, omega_z) = sensors.get_angular_velocity()
    print("Time:{0:.2f}\t\tAng. vel x:{1:.2f}\t\ty:{2:.2f}\t\tz:{3:.2f}".format(time, omega_x, omega_y, omega_z))

    (time, theta_x, theta_y, theta_z) = sensors.get_angular_displacement()
    print("Time:{0:.2f}\t\tAngle x:{1:.2f}\t\ty:{2:.2f}\t\tz:{3:.2f}".format(time, theta_x, theta_y, theta_z))

if __name__ == "__main__":
    # Testing function. Type in commands
    while True:
        print_data()
        time.sleep(DELAY)

