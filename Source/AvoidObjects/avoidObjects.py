#This program will control a six-wheel Mantis robot autonomously
#to move around (a relatively open room) without colliding with objects
#using a HC-SR04 ultrasonic sensor

import ultrasonic
import time
from mantis import Mantis

#in centimetres
MAX_DIST_TO_STOP = 10
MAX_DIST_TO_TURN = 50
SPAN_DIST_TO_TURN = (MAX_DIST_TO_TURN - MAX_DIST_TO_STOP)

#in seconds
WAIT_TIME_INTERVAL = 0.1
WAIT_PERPENDICULAR = 2

NORMAL_VELOCITY_SIZE = 0.5
MIN_VELOCITY_SIZE_PROPORTION = 0.4

robot = Mantis()

# Wrap main content in a try block so we can
# catch the user pressing CTRL-C and run the
# GPIO cleanup function. This will also prevent
# the user seeing lots of unnecessary error
# messages.

try:
    velocitySize = NORMAL_VELOCITY_SIZE
    turnSize = 0
    
    while True:
        distance = ultrasonic.get_distance()
        print ("Distance reading: %.lf" % distance)
        
        if distance < MAX_DIST_TO_STOP:
            print ("Stop and reverse")
            #stop moving
            robot.set_velocity_size(0)
            
            #turn and reverse
            turnSize = 1
            robot.set_turn_size(turnSize)
            robot.set_velocity_size(-NORMAL_VELOCITY_SIZE)
            time.sleep(WAIT_PERPENDICULAR)

            #reset
            turnSize = 0
            velocitySize = NORMAL_VELOCITY_SIZE
        elif distance < MAX_DIST_TO_TURN:
            print ("Slowly turn")
            
            #turn size is proportional to closeness of object
            turnSize = 1 - (distance - MAX_DIST_TO_STOP)/SPAN_DIST_TO_TURN
            
            #velocity size inversely propritional to distance to object,
            #max velocity size is NORMAL_VELOCITY_SIZE
            #min velocity size is MIN_VELOCITY_SIZE_PROPORTION*NORMAL_VELOCITY_SIZE
            velocitySize = NORMAL_VELOCITY_SIZE*(MIN_VELOCITY_SIZE_PROPORTION + (1 - MIN_VELOCITY_SIZE_PROPORTION)*((distance - MAX_DIST_TO_STOP)/SPAN_DIST_TO_TURN))
        else:
            print ("Go straight")
            turnSize = 0
            velocitySize = NORMAL_VELOCITY_SIZE
        
        print("Turn size: {}\t\tVelocity size: {}\n\n".format(turnSize, velocitySize))
        robot.set_turn_size(turnSize)
        robot.set_velocity_size(velocitySize)
        
        time.sleep(WAIT_TIME_INTERVAL)

except KeyboardInterrupt:
  # User pressed CTRL-C
  # Reset GPIO settings
  print("Keyboard Interrupt\n")
  robot.set_velocity_size(0)
  robot.set_turn_size(0)
  ultrasonic.cleanup()





