#RoboRescue AvoidObjects project version 1.0 04/06/2015

##PREREQUISITES
See roboRescueReport.pdf in the root directory for detailed instructions on how to set up the raspberry pi so that the python scripts will work

##DIRECTORY STRUCTURE
###Xcode specific files
/RoboRescue.xcodeproj
/DerivedData

These can be ignored if not editing code using Xcode

###Avoid Objects program files:
/Source/AvoidObjects

- avoidObjects.py is the working avoid objects script
- avoidObjectsAdvanced.py was our exploration into using the accel/gyro to make more complex decisions. This script does not work/hasn't been tested, as we the motion.py gyro/accelerometer readings didn't seem reliable, and we decided that adding gyro/accelerometer based decisions would introduce extra complexity for little added functionality
- motion.py defines a Motion object which constantly polls the GY521 accelerometer/gyroscope sensor for acceleration, and integrates angular velocity for angular displacement. The acceleration and angular velocity readings are reliable, the angular displacement readings for x and y rotations are reliable, but the z rotations sometimes are unreliable.
- motionInterface.py uses the Motion object to repeatedly print the read acceleration, angular velocity and angular displacement values. Used to test motion.py.
- ultrasonic.py contains the necessary functions to take distance measurements using a HC-SR04 ultrasonic sensor. This script works reliably.
- ./mantis includes the files from the third year mantis project which controls the dynamixel motors on a 6-wheeled mantis robot. We made some small changes to mantis.py so that it would run

###Dynamixel motor related files:
/Source/Dynamixel

- ./mantis contains a clone of the third year Mantis project from github (https://github.com/icedtrees/mantis) as well as their project report (http://robolab.cse.unsw.edu.au/~mmcgill/comp3431/assignment2/Mini%20Rescue/report.pdf) called pyoarkitReport.pdf. We made some modifications to mantis.py and interface.py to get it to work.
- ./pydynamixel contains a clone of the pydynamixel library used in the mantis project from github (https://github.com/iandanforth/pydynamixel). The example.py file is useful for checking that the dynamixel network is correctly wired and working. The USB port, on a raspberry pi, should be '/dev/ttyUSB0' which is the upper left USB port of the four usb ports. If on a mac, it will be similar to '/dev/tty.usbserial-AH01FOW4'. The baud rate is the default, and the highest servo ID is 19. The output servo ID numbers should match those in config.py in the mantis folder.

###GY521 (MPU-6050) accelerometer/gyro files
/Source/GY521
These are sample python scripts which we used to test that our sensor worked and as a starting point for motion.py. They were based on the tutorials at:
- http://blog.bitify.co.uk/2013/11/reading-data-from-mpu-6050-on-raspberry.html
- http://blog.bitify.co.uk/2013/11/using-complementary-filter-to-combine.html

###HC-SR04 ultrasonic files
/Source/HC-SR04
These are sample python scripts which we used to test that our sensor worked and as a starting point for ultrasonic.py. They were based on tutorials at:
- http://www.raspberrypi-spy.co.uk/2012/12/ultrasonic-distance-measurement-using-python-part-1/
- http://www.raspberrypi-spy.co.uk/2013/01/ultrasonic-distance-measurement-using-python-part-2/
- http://www.bytecreation.com/blog/2013/10/13/raspberry-pi-ultrasonic-sensor-hc-sr04
